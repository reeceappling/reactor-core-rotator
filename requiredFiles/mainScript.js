let indent = [4,2,1,1,0,0,0,0,0,0,0,1,1,2,4];
var hasWebStorage=false,cookiesEnabled,storageType=null;
var recentInText,recentOutText;
var rotatable=0,nRotd=[0,0,0,0];
var core=[];
var orLab=[];
const letArr=["A","B","C","D","E","F","G","H","J","K","L","M","N","P","R"];


const cellSize = 3;//vmin

//account for webkit and mozilla boxshadows-----------------------------------------------
//fix boxshadow in table -----------------------------------------------------------------
	//dynamically implement side-shadowing
//button to black out unclickable--------------------------------------------------------
//random solution solver -----------------------------------------------------------------
//Output Core ----------------------------------------------------------------------------
//import and color based on burnup --------------------------------------------------------
//dynamically size core ------------------------------------------------------------------

function dropIn(e){
	if (e.preventDefault) { e.preventDefault(); }
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	switch(files.length){
		case 1:
			if(files[0].name.split('.')[1]=='inp'){
				loadInFromFile(files[0],true);
			}
			else{
				//not input file, notify -------------------
			}
			break;
		case 2:
			var temp=[0,0,0];
			for (var i = 0, f; f = files[i]; i++) {
				if(files[i].name.split('.')[1]=='inp'&&temp[1]==0){
					temp[0]++;
					temp[1]=i;
				}
				if(files[i].name.split('.')[1]=='out'&&temp[2]==0){
					temp[0]++;
					temp[2]=i;
				}
			}
			if(temp[0]==2){
				//both files exist ---------------------------
				loadInFromFile(files[temp[1]],true);
				loadOutFromFile(files[temp[2]])
			}
			else{
				//at least one file incorrect -----------------------
			}
			break;
		default:
			//too many or too little files, notify ------------------
	}
	return false;
}

function dragoverIn(e){
	e.preventDefault();
	if(containsFiles(e)){
		//DO STUFF------------------
		//CHANGE BOX SHADOW --------------------
	}
}
function dragendIn(e){
	e.preventDefault();
}

function containsFiles(ev){
    if(ev.dataTransfer.types){
        for(var i=0;i<ev.dataTransfer.types.length;i++){if(ev.dataTransfer.types[i] == "Files"){return true;}}
	}    
	return false;
}

function dropOut(e){
	if (e.preventDefault) { e.preventDefault(); }
	// fetch FileList object
	var files = e.target.files || e.dataTransfer.files;
	if(files[0].name.split('.')[1]=='out'){
		loadOutFromFile(files[0]);
	}
	else{
		//not output file, notify -------------------
	}
	return false;
}

function loadInFromFile(file,toLogin){
	//read file as text
	var reader = new FileReader();
	var fxn = function(e){
		recentInText = reader.result;
		//read FUE.NEW or FUE.TYP
		var fueNew=[];
		var rawFUE=reader.result.split('\'FUE.NEW\'').slice(1);
		//for input files with FUE.NEW
		if(rawFUE.length>2){//MAY WANT TO CHANGE 2????? IDK----------------------------------------------
			generateCoreDOM();
			for(var i=0;i<rawFUE.length;i++){fueNew.push(rawFUE[i].split(',,1/')[0].match(/\s{1}\'{1}\S+\'{1}\,{1}\s{1}\'{1}/g)[0].match(/[^\'\s\,]\S+[^\'\s\,]/g)[0]);}
			//load labels into core
			var rawlabels = reader.result.split('\'FUE.LAB\' 6/')[1].split('FUE.LAB/SER OR BPR.SER')[0].split("\n");
			for(var i=1;i<=15;i++){
				var labRowArr=[];
				var rowlabelarray = rawlabels[i].match(/\S{6}/g);
				//Loop through all labels
				for(var j=0;j<rowlabelarray.length;j++){
					//If assembly is non-rotatable
					if(fueNew.includes(rowlabelarray[j])){
						$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+') p.rotVal').html("&#x2716;");
						$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+')').css("background-color","white");
						$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+')').addClass("noHover");
						core[i-1][indent[i-1]+j].isNew=true;
						rotatable=rotatable-1;
						nRotd[0]=nRotd[0]-1;
					}
					//if assembly is rotatable
					else{
						$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+') p.rotVal').html("0&#xb0;");
						core[i-1][indent[i-1]+j].isNew=false;
						$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+')').addClass("pointHover");
						//var tcol=letArr.indexOf(rowlabelarray[j].CharAt(2));
						//var trow=Math.Number(rowlabelarray[j].split('-')[1])-1;
						//core[i-1][indent[i-1]+j].exp=;
					}
					core[i-1][indent[i-1]+j].label=rowlabelarray[j];
					core[i-1][indent[i-1]+j].labelNew=rowlabelarray[j];
					$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+') p.iLab').html(rowlabelarray[j]);
					$('.fullcoretable tr:nth-child('+(i)+') td:nth-child('+(indent[i-1]+j+1)+') p.fLab').html(rowlabelarray[j]);
				}
			}
			//read original fue.lab placements
			var arLab=reader.result.split("'COM''FUE.LAB' 6/")[1].split('\n').slice(1);
			for(var i=0;i<15;i++){
				orLab.push({lab:arLab[i].split("'COM'")[1].match(/\S{6}/g),exp:null});//-------------------------------
			}
			//console.log(orLab);
			$('.fullcoretable tr:nth-child(8) td:nth-child(8) p.rotVal').html("&#x2716;");
			$('.fullcoretable tr:nth-child(8) td:nth-child(8)').css('border-width','2px');
			dataTabUpdate();
		}
		else{
			rawFUE=reader.result.split('\n');
			let tempRowArr=[];
			for(var i=0;i<rawFUE.length;i++){if(rawFUE[i].match(/^\'FUE.TYP'/g)){tempRowArr.push(rawFUE[i].split(',')[1].split('/')[0]);}}
			rawFUE = tempRowArr; tempRowArr = [];
			for(var i=0;i<rawFUE.length;i++){fueNew.push(rawFUE[i].match(/[0-9]+/g));}
			for(var i=0;i<fueNew.length;i++){
				let k=0;
				for(var j=0;j<fueNew[0].length;j++){if(fueNew[fueNew.length-1-i][j]=='0'){k++};}
				tempRowArr.push(k);
			}
			let tempNum=tempRowArr.length;
			for(var i=1;i<tempNum;i++){
				tempRowArr.push(tempRowArr[tempNum-1-i]);
			}
			indent = tempRowArr;
			console.log('outputting coresize')
			console.log(fueNew);
			generateCoreDOM();
		}
		//For files with FUE.TYP
		//* WILL WANT TO READ FUE.TYP LIKE NEW PROJECT INSTEAD OF FUE.NEW---------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//*-----------------------------------------------------------------------------------------------------------------------------------------*//
		//move away from inputPage if toLogin is true
		if(toLogin){
			$("#inputPage").slideUp({duration:1000});
		}
	}
	reader.onload = fxn;
	if(file==="example"){
		reader = {};
		reader.result = exampleTemplate;
		fxn();
		loadOutFromFile('example');
	}else{
		reader.readAsText(file);
	}
}

function loadOutFromFile(file){
	//read file as text
	var reader = new FileReader();
	
	let tmpFx = function(e) {
		//read labelmap
		//-----------------------------------
		//read peak pin exposures
		var rawexp=reader.result.split('PIN.EDT 2XPO  - Peak Pin Exposure (GWd/T):   Assembly 2D  (AFTER PINFIL INTEGRATION)');
		rawexp=rawexp[rawexp.length-1].split(' **   H-     G-     F-     E-     D-     C-     B-     A-     **')[0].split('**    8      9     10     11     12     13     14     15     **')[1].split('\n').slice(1);
		var exps=[];
		for(var i=0;i<rawexp.length-1;i++){
			exps.push(rawexp[i].match(/\d{2}\.\d{3}/g));
		}
		for(var i=0;i<15;i++){
			for(var j=0;j<15;j++){
				if(core[i][j].exists){
					if(core[i][j].isNew==false){
						//Math.abs(8-Number(core[i][j].label.split('-')[1]));//row
						//Math.abs(7-letArr.indexOf(core[i][j].label.split('-')[0].slice(2)));//col
						core[i][j].exp=Number(exps[Math.abs(8-Number(core[i][j].labelNew.split('-')[1]))][Math.abs(7-letArr.indexOf(core[i][j].labelNew.split('-')[0].slice(2)))]);
						//$(core[i][j].cellref).css('background-color','rgb(255,0,255)');
						$(core[i][j].cellref).css('background-color','rgb('+Math.round(core[i][j].exp*255/80)+',255,0)');
					}
				}
			}
		}
		//-----------------------------------
		
	}
	reader.onload = tmpFx;
	if(file==="example"){
		reader = {};
		reader.result = exampleTemplate2;
		tmpFx();
	}else{
		reader.readAsText(file);
	}
}

function maketable(div){
	var drows = "";
	for(var i=0;i<15;i++){var dcells = "";
		for(var k=0;k<15;k++){dcells = dcells + "<td class=\"fccell\"><div class='cellData'><p class='iLab cdat'></p><center><p class='rotVal cdat'></p></center><p class='fLab cdat'></p></div></td>";}
		drows = drows+"<tr>"+dcells+"</tr>";
	}
	$(div).append("<table id=\""+div+"\"class=\"fullcoretable\">"+drows+"</table>");
}

//black out a given cell and hide it in background
function blackout(cell){
	$(cell).css("background-color","white");
	$(cell).css("border","none");
	$(cell).css("z-index",-999);
	$(cell).css("visibility",'hidden');
	core[$(cell).parent().index()][$(cell).index()].exists=false;
}

//changes rotational values
function rotate(clockwise,grp){
	if(core[grp[0].r][grp[0].c].isNew==false){
		var rotTo;
		var rotFrom=core[grp[0].r][grp[0].c].rot;
		var nGrp;
		if(clockwise){
			var nGrp=[core[grp[1].r][grp[1].c].labelNew,core[grp[2].r][grp[2].c].labelNew,core[grp[3].r][grp[3].c].labelNew,core[grp[0].r][grp[0].c].labelNew];
			var nGrpExp=[core[grp[1].r][grp[1].c].exp,core[grp[2].r][grp[2].c].exp,core[grp[3].r][grp[3].c].exp,core[grp[0].r][grp[0].c].exp];
			if(rotFrom==0){rotTo=3;}else{rotTo=rotFrom-1;}
		}else{
			var nGrp=[core[grp[3].r][grp[3].c].labelNew,core[grp[0].r][grp[0].c].labelNew,core[grp[1].r][grp[1].c].labelNew,core[grp[2].r][grp[2].c].labelNew];
			var nGrpExp=[core[grp[3].r][grp[3].c].exp,core[grp[0].r][grp[0].c].exp,core[grp[1].r][grp[1].c].exp,core[grp[2].r][grp[2].c].exp];
			if(rotFrom==3){rotTo=0;}else{rotTo=rotFrom+1;}}
		for(var i=0;i<4;i++){
			core[grp[i].r][grp[i].c].rot=rotTo;
			core[grp[i].r][grp[i].c].labelNew=nGrp[i];
			core[grp[i].r][grp[i].c].exp=nGrpExp[i];
			$(core[grp[i].r][grp[i].c].cellref+" p.fLab").html(nGrp[i]);
			$(core[grp[i].r][grp[i].c].cellref+" p.rotVal").html(rotTo*90+"&#xb0;");
			$(core[grp[i].r][grp[i].c].cellref).css('background-color','rgb('+Math.round(nGrpExp[i]*255/80)+',255,0)');
		}
		nRotd[rotFrom]=nRotd[rotFrom]-4;nRotd[rotTo]=nRotd[rotTo]+4;dataTabUpdate();
	}
}

function quarter(roww,coll){
	if(roww<7&&coll>=7){return 0;}
	else if(roww<8&&coll<7){return 1;}
	else if(roww>7&&coll<8){return 2;}
	else if(roww>6&&coll>7){return 3;}
	else {return 4;} //accounts for center
}

function qRow(r,c){
	switch(quarter(r,c)){
		case 0:return r;break;
		case 1:return c;break;
		case 2:return 14-r;break;
		case 3:return 14-c;break;
		default:return 0;
	}
}

function qCol(r,c){
	switch(quarter(r,c)){
		case 0:return c-7;break;
		case 1:return 7-r;break;
		case 2:return 7-c;break;
		case 3:return r-7;break;
		default:return 0;
	}
}

function group(ir,ic){
	var qr=qRow(ir,ic),qc=qCol(ir,ic);
	return [{r:qr,c:qc+7},{r:7-qc,c:qr},{r:14-qr,c:7-qc},{r:7+qc,c:14-qr}];
}

function dataTabUpdate(){
	var numcorrect=0;
	for(var i=0;i<4;i++){
		$('#rotTab tr:nth-child(2) td:nth-child('+(i+2)+')').html(nRotd[i]);
		$('#rotTab tr:nth-child(3) td:nth-child('+(i+2)+')').html(rotatable/4);
		if(nRotd[i]==rotatable/4){
			$('#rotTab tr:nth-child(2) td:nth-child('+(i+2)+')').css('color','green');
			numcorrect++;
		}
		else{$('#rotTab tr:nth-child(2) td:nth-child('+(i+2)+')').css('color','red');}
	}
	if(numcorrect==4){$('#invisButton').show();$('#solveAll').hide();}
	else{$('#invisButton').hide();$('#solveAll').show();}
}

function gencore(){
	for(var i=0;i<15;i++){
		var temprow=[];
		for(var j=0;j<15;j++){
			temprow.push({
				cellref:'.fullcoretable tr:nth-child('+(i+1)+') td:nth-child('+(j+1)+')',
				exists:null,
				burnup:null,
				isNew:false,
				rot:0,
				label:null,
				labelNew:null,
				quarter:quarter(i,j),
				qRow:qRow(i,j),
				qCol:qCol(i,j),
				group:group(i,j),
				exp:0
			});
		}
		core.push(temprow);
	}
}

function toClip(){
	var toOut='\'FUE.LAB\' 6/';
	for(var i=0;i<15;i++){
		var tempstr='\n';
		if(i<9){tempstr="\n "}
		tempstr=tempstr+(i+1)+"  1";
		for(var j=0;j<15;j++){
			if(core[i][j].exists==true){tempstr=tempstr+" "+core[i][j].labelNew;}
			else{tempstr=tempstr+"       ";}
		}
		toOut=toOut+tempstr;
	}
	toOut=toOut+'\n0  0     FUE.LAB/SER OR BPR.SER\n\'COM\'/';
	$('#outarea').html(toOut);
	$('#outContainer').slideDown();
}

function autoSolve(){
	for(var j=0;j<15;j++){
		for(var k=0;k<15;k++){
			if(core[j][k].exists==true){
				if((nRotd[core[j][k].rot]>(rotatable/4))&&(core[j][k].isNew==false)&&!(j==7&&k==7)){
					rotate(true,group(j,k));
					if(hasOpposite(j,k)){rotate(false,core[k][j].group);}
				}
			}
		}
	}
}

function hasOpposite(r,c){
	var gEq=group(r,c)[1];
	if(gEq.r==7||(gEq.r==gEq.c)){return false;}
	else{return true;}
}

function generateCoreDOM(){
	maketable('#Core');//generate core table
	gencore();//generate core object
	//blackout, resize, position core table
	$('.fullcoretable tr td').each(function(){
		//$(this).css('height',/*'3.5em'*/Screen.height/20);
		//$(this).css('width',/*'3.5em'*/Screen.height/20);
		//$(this).css('height',cellSize+'vmin');
		//$(this).css('width',cellSize+'vmin');
		if(Math.abs($(this).parent().index()-7)+Math.abs($(this).index()-7)>=11&&!(Math.abs($(this).parent().index()-7)==6&&Math.abs($(this).index()-7)==5)&&!(Math.abs($(this).parent().index()-7)==5&&Math.abs($(this).index()-7)==6)){
			blackout(this);
		}
		else{
			$(this).css('z-index','100');
			//change corresponding assembly exists value to false
			core[$(this).parent().index()][$(this).index()].exists=true;
			if(!($(this).parent().index()==7&&$(this).index()==7)){rotatable++;nRotd[0]++}
			$(this).contextmenu(function(){
				if(!(($(this).parent().index()==7)&&($(this).index()==7))){
					rotate(true,core[$(this).parent().index()][$(this).index()].group);
					if(hasOpposite($(this).parent().index(),$(this).index())){
						rotate(false,core[$(this).index()][$(this).parent().index()].group);
					}
					return false;
				}
			});
			$(this).click(function(){
				if(!(($(this).parent().index()==7)&&($(this).index()==7))){
					rotate(false,core[$(this).parent().index()][$(this).index()].group);
					if(hasOpposite($(this).parent().index(),$(this).index())){
						rotate(true,core[$(this).index()][$(this).parent().index()].group);
					}
				}
			});
		}
	});
	dataTabUpdate();
	$('#Core').css('top','5px');
}


//on document ready
$(function(){
	$('#outContainer').slideUp();
	for(var i=1;i<4;i++){$('#rotTab tr:nth-child('+(i+1)+') td:nth-child(6)').css('border-color','white');}
	//add event listeners
	document.getElementById('infileselect').addEventListener('change',dropIn,false);
	document.getElementById('outfileselect').addEventListener('change',dropOut,false);
	$('#CopyMe').click(function(){toClip();});
	$('#solveAllContainer').click(function(){autoSolve();});
	//check for cookie support
	cookiesEnabled=navigator.cookieEnabled;
	//check for web storage support
	if(typeof(Storage)!=="undefined"){
		hasWebStorage=true;
		//check for existing data in webstorage
		if(localStorage.getItem("coreRotator")===null){
			if(sessionStorage.getItem("coreRotator")===null){
				if(cookiesEnabled){//if cookies enabled, search cookie
					if(getCookie("coreRotator")!==null){
						//---------------------------------------------
					}
					else{//show #input_storePrompt and #input_cookiePrompt
						//---------------------------------------------
					}
				}
				else{//if cookies are not enabled, show #input_storeUse
					//---------------------------------------------
				}
			}
		}
		else if(cookiesEnabled){//if cookies are enabled but not webstorage, check for cookie
			if(getCookie("coreRotator")!==null){
				//---------------------------------------------
			}
			else{//prompt to use cookies, show #input_cookiePrompt
				//---------------------------------------------
			}
		}
		else{//if neither cookies nor webstorage is available, show #input_noStore
			//---------------------------------------------
		}
	}
});