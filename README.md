# Reactor Core Rotator

Reactor core fuel assembly rotation tool for SIMULATE input files

![Rotator Image](images/Capture.PNG)

Note: this program has not yet been extended to arbitrary reactor geometries

# Access

- **Download** this repository and open shuffler.html in a browser
- link to the **[live copy](https://reeceappling.gitlab.io/reactor-core-rotator/coreRotator.html)** of the most current commit.

# Usage

## Loading Data

Once open, 1 .inp file must be loaded in which can be done 3 ways:

- Drag and drop
- File select button
- Using the **Default** link

A .OUT file can also be loaded at the same time as the .inp or afterwards



## Rotating assemblies

- Left click boxes to rotate them 90 degrees clockwise
- right click to go the opposite direction
- There is an automatic rotation button to expedite the process

## Output

- Once optimal rotations have been acahieved, the Output button will be shown
- Clicking the Output button will reveal the text to be saved as an .inp file